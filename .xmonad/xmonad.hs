  -- Base --
import XMonad
import System.Directory
import System.IO (hPutStrLn)
import System.Exit (exitSuccess)
import qualified XMonad.StackSet as W

import XMonad.Hooks.ManageDocks
import XMonad.Actions.UpdatePointer
import Control.Monad (liftM2)
import XMonad (windows, X, WindowSet, XState(XState, windowset))
import Control.Monad.State

    -- Actions --
import XMonad.Actions.CopyWindow (kill1)
import XMonad.Actions.CycleWS (Direction1D(..), moveTo, shiftTo, WSType(..), nextScreen, prevScreen)
import XMonad.Actions.GridSelect
import XMonad.Actions.MouseResize
import XMonad.Actions.Promote
import XMonad.Actions.RotSlaves (rotSlavesDown, rotAllDown)
import XMonad.Actions.WindowGo (runOrRaise)
import XMonad.Actions.WithAll (sinkAll, killAll)
import qualified XMonad.Actions.Search as S

    -- Data --
import Data.Monoid
import Data.Maybe (fromJust)
import qualified Data.Map as M

    -- X11 --
import Graphics.X11.ExtraTypes.XF86

    -- Layouts --
import XMonad.Layout.Accordion
import XMonad.Layout.GridVariants (Grid(Grid))
import XMonad.Layout.SimplestFloat
import XMonad.Layout.Spiral
import XMonad.Layout.ResizableTile
import XMonad.Layout.Tabbed
import XMonad.Layout.ThreeColumns

    -- Layouts modifiers --
import XMonad.Layout.LayoutModifier
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.Magnifier
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import XMonad.Layout.NoBorders
import XMonad.Layout.Renamed
import XMonad.Layout.ShowWName
import XMonad.Layout.Simplest
import XMonad.Layout.Spacing
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..))
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))

    -- Utilities --
import XMonad.Util.Run
import XMonad.Util.SpawnOnce
import XMonad.Util.EZConfig

  -- VARIABLE --

myModMask :: KeyMask
myModMask = mod4Mask

myAltMask :: KeyMask
myAltMask = mod1Mask

myTerminal :: String
myTerminal = "alacritty"

myBrowser :: String
myBrowser = "librewolf-nightly"

myEmailClient :: String
myEmailClient = "~/.AppImages/criptext.AppImage"

myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

myClickJustFocuses :: Bool
myClickJustFocuses = False

myBorderWidth :: Dimension
myBorderWidth = 2

myNormalBorderColor :: String
myNormalBorderColor = "#2E3440"

myFocusedBorderColor :: String
myFocusedBorderColor = "#434C5E"

myWorkspaces    = ["1:www","2:vid","3:dev","4:sys","5:doc","6:VM","7:chat","8:mus","9:gfx"]
myWorkspaceIndices = M.fromList $ zipWith (,) myWorkspaces [1..] -- (,) == \x y -> (x,y)

clickable ws = "<action=xdotool key super+"++show i++">"++ws++"</action>"
    where i = fromJust $ M.lookup ws myWorkspaceIndices

------------------------------------------------------------------------

 -- KEY BINDINGS --
myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $

    -- launch a terminal
    [ ((modm, 	            xK_Return), spawn $ XMonad.terminal conf)

    -- launch browser
    , ((modm,               xK_b     ), spawn "librewolf-nightly")
    
    -- launch rofi
    , ((modm,               xK_space ), spawn "rofi -show run")

    -- Mic mute
    , ((0,        xF86XK_AudioMicMute), spawn "pactl set-source-mute 0 toggle")

    -- Raise volume
    , ((0,    xF86XK_AudioRaiseVolume), spawn "amixer -D pulse sset Master 10%+")

    -- Lower volume
    , ((0,    xF86XK_AudioLowerVolume), spawn "amixer -D pulse sset Master 10%-")

    -- Mute volume
    , ((0,           xF86XK_AudioMute), spawn "amixer -D pulse sset Master toggle")

    -- Raise brightness
    , ((0,     xF86XK_MonBrightnessUp), spawn "brightnessctl set +5%")

    -- Lower brightness
    , ((0,   xF86XK_MonBrightnessDown), spawn "brightnessctl set 10%-")

    -- close focused window
    , ((modm, 	            xK_k     ), kill)

     -- Rotate through the available layout algorithms
    , ((modm,               xK_n     ), sendMessage NextLayout)

    --  Reset the layouts on the current workspace to default
    , ((modm, 	            xK_r     ), setLayout $ XMonad.layoutHook conf)

    -- Resize viewed windows to the correct size
    , ((modm,               xK_n     ), refresh)

    -- Move focus to the next window
    , ((modm,               xK_d     ), windows W.focusDown)

    -- Move focus to the previous window
    , ((modm,               xK_u     ), windows W.focusUp  )

    -- Move focus to the master window
    , ((modm,               xK_m     ), windows W.focusMaster  )

    -- Swap the focused window and the master window
    , ((modm,               xK_s     ), windows W.swapMaster)

    -- Swap the focused window with the next window
    , ((modm .|. shiftMask, xK_d     ), windows W.swapDown  )

    -- Swap the focused window with the previous window
    , ((modm .|. shiftMask, xK_u     ), windows W.swapUp    )

    -- Shrink the master area
    , ((modm,               xK_h     ), sendMessage Shrink)

    -- Expand the master area
    , ((modm,               xK_e     ), sendMessage Expand)

    -- Push window back into tiling
    , ((modm,               xK_t     ), withFocused $ windows . W.sink)

    -- Increment the number of windows in the master area
    , ((modm              , xK_comma ), sendMessage (IncMasterN 1))

    -- Deincrement the number of windows in the master area
    , ((modm              , xK_period), sendMessage (IncMasterN (-1)))

    -- Quit xmonad
    , ((modm .|. mod1Mask,   xK_q    ), io (exitWith ExitSuccess))

    -- Restart xmonad
    , ((modm .|. mod1Mask,   xK_r    ), spawn "xmonad --recompile; xmonad --restart")]

------------------------------------------------------------------------

 -- WORKSPACE BINDINGS --
    ++

    [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
    ++

    [((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]


------------------------------------------------------------------------

 -- MOUSE BINDINGS --
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    ]

------------------------------------------------------------------------

 -- LAYOUTS --
myLayoutHook = avoidStruts $ mouseResize $ windowArrange $ T.toggleLayouts floats
               $ mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
             where
               myDefaultLayout =     withBorder myBorderWidth tall
                                 ||| magnify
                                 ||| noBorders monocle
                                 ||| floats
                                 ||| noBorders tabs
                                 ||| grid
                                 ||| spirals
                                 ||| threeCol
                                 ||| threeRow
                                 ||| tallAccordion
                                 ||| wideAccordion

------------------------------------------------------------------------

 -- WINDOW RULES --
myManageHook = composeAll
    [ className =? "Librewolf"               --> viewShift "1:www"
    , className =? "Freetube"                --> viewShift "2:vid"
    , className =? "Emacs"                   --> viewShift "3:dev"
    , className =? "Alacritty"               --> viewShift "4:sys"
    , className =? "Virtual Machine Manager" --> viewShift "6:VM"
    , className =? "Criptext"                --> viewShift "7:chat"
    , className =? "firefox"   --> doShift ( myWorkspaces !! 1 ) 
    , className =? "Dunst"                   --> doFloat
    , className =? "lxqt-policykit-agent"    --> doFloat
    , resource  =? "desktop_window"          --> doIgnore
    , resource  =? "kdesktop"                --> doIgnore ]
    where viewShift = doF . liftM2 (.) W.greedyView W.shift

------------------------------------------------------------------------

 -- Event handling
myEventHook = mempty

------------------------------------------------------------------------

 -- STATUS BARS --
myLogHook = return ()

------------------------------------------------------------------------

 -- STARTUP HOOK --
myStartupHook = do
	spawnOnce "setxkbmap -layout gb &"
        spawnOnce "xwallpaper --zoom ~/Pictures/wallpapers/Wallpapers/0094.png &"
	spawnOnce "picom --experimental-backends &"
        spawnOnce "mpd"
        spawnOnce "~/.config/polybar/launch.sh"
	spawnOnce "lxqt-policykit-agent &"


------------------------------------------------------------------------

 -- XMONAD DEFAULTS --
main = do
  xmproc <- spawnPipe "xmobar -x 0 ~/.config/xmobar/xmobarrc"
  xmonad $ docks defaults

------------------------------------------------------------------------

 -- CONFIGURATION SETTINGS --
defaults = def {
      -- simple stuff
        terminal           = myTerminal,
        focusFollowsMouse  = myFocusFollowsMouse,
        clickJustFocuses   = myClickJustFocuses,
        borderWidth        = myBorderWidth,
        modMask            = myModMask,
        workspaces         = myWorkspaces,
        normalBorderColor  = myNormalBorderColor,
        focusedBorderColor = myFocusedBorderColor,

      -- key bindings
        keys               = myKeys,
        mouseBindings      = myMouseBindings,

      -- hooks, layouts
        layoutHook         = myLayout,
        manageHook         = myManageHook,
        handleEventHook    = myEventHook,
        logHook            = myLogHook,
        startupHook        = myStartupHook
    }
