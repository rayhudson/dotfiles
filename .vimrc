call plug#begin()
Plug 'arcticicestudio/nord-vim'
Plug 'vim-airline/vim-airline'
call plug#end()

colorscheme nord

filetype plugin indent on
syntax on

let g:airline_powerline_fonts = 1
set t_Co=256
