# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
if [ -d ~/.bashrc.d ]; then
	for rc in ~/.bashrc.d/*; do
		if [ -f "$rc" ]; then
			. "$rc"
		fi
	done
fi

unset rc

# Functions

cd() {
    builtin cd "$@" && ls -lA
}

..() {
    cd ..
}

ip() {
    curl https://ifconfig.me/ip
}

extract() {
    if [ -z ${1} ] || [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
        echo "Usage: extract <archive> [directory]"
        echo "Example: extract presentation.zip."
        echo "Valid archive types are:"
        echo "tar.bz2, tar.gz, tar.xz, tar, bz2, gz, tbz2,"
        echo "tbz, tgz, lzo, rar, zip, 7z, xz, txz, lzma and tlz"
    else
        case "$1" in
            *.tar.bz2|*.tbz2|*.tbz)         tar xvjf "$1" ;;
            *.tgz)                          tar zxvf "$1" ;;
            *.tar.gz)                       tar xvzf "$1" ;;
            *.tar.xz)                       tar xvJf "$1" ;;
            *.tar)                          tar xvf "$1" ;;
            *.rar)                          7z x "$1" ;;
            *.zip)                          unzip "$1" ;;
            *.7z)                           7z x "$1" ;;
            *.lzo)                          lzop -d  "$1" ;;
            *.gz)                           gunzip "$1" ;;
            *.bz2)                          bunzip2 "$1" ;;
            *.Z)                            uncompress "$1" ;;
            *.xz|*.txz|*.lzma|*.tlz)        xz -d "$1" ;;
            *) echo "Sorry, '$1' could not be decompressed." ;;
        esac
    fi
}

commit() {
     git commit --gpg-sign=KEY -m *
}

status() {
     git status
}

add() {
     git add *
}

config() {
     /usr/bin/git --git-dir=$HOME/Projects/dotfiles/ --work-tree=$HOME "$@"
}

weather() {
      curl wttr.in/London
}

getnews() {
     curl https://newsapi.org/v2/top-headlines -s -G -d sources=$1 -d apiKey=068d2a6b5dbb4d399e0a835fd872e913 | jq -r '.articles[] | .title, .url, ""'
}

startmyday () {
  echo "Good morning, Ray."
  echo "\nUpdating Pacman..."
  sudo pacman -Syu
  echo "\nThe weather right now:"
  weather
  echo "\nNews from wikileaks"
  getnews wikileaks
  echo "\nNews from Hacker News:"
  getnews hacker-news
}

# Aliases

alias pip=”$HOME/.local/bin/pip”

# Exports

export PATH="~/.local/bin:$PATH"

# Fetch Master 6000

fm6000 --file ~/.local/bin/arch.txt --os GNU/Linux -not_de --shell Bash --color blue --length 25 --margin 8 --gap 8

# Prompt

if [ -f `which powerline-daemon` ]; then
  powerline-daemon -q
  POWERLINE_BASH_CONTINUATION=1
  POWERLINE_BASH_SELECT=1
  . /usr/share/powerline/bash/powerline.sh
fi
